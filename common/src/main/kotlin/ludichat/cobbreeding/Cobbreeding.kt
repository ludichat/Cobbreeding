package ludichat.cobbreeding

import com.cobblemon.mod.common.platform.events.PlatformEvents
import com.mojang.logging.LogUtils.getLogger
import dev.architectury.registry.ReloadListenerRegistry
import dev.architectury.registry.registries.DeferredRegister
import dev.architectury.registry.registries.RegistrySupplier
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.decodeFromStream
import kotlinx.serialization.json.encodeToStream
import ludichat.cobbreeding.components.CobbreedingComponents
import net.minecraft.core.registries.Registries
import net.minecraft.resources.ResourceLocation
import net.minecraft.server.packs.PackType
import net.minecraft.server.packs.resources.ResourceManagerReloadListener
import net.minecraft.world.item.Item
import net.minecraft.world.item.ItemStack
import org.slf4j.Logger
import java.io.File

object Cobbreeding {
    const val MOD_ID = "cobbreeding"

    private const val CONFIG_PATH = "config/cobbreeding/main.json"

    internal val ITEMS: DeferredRegister<Item> = DeferredRegister.create(MOD_ID, Registries.ITEM)

    @JvmField
    val LOGGER: Logger = getLogger()

    @JvmField
    val EGG_ITEM: RegistrySupplier<PokemonEgg> = ITEMS.register("pokemon_egg") {
        PokemonEgg(Item.Properties().stacksTo(1))
    }

    // Cache per players indicating if their egg should hatch faster
    val INCUBATOR_ABILITIES_REGISTRY: IncubatorAbilitiesRegistry = IncubatorAbilitiesRegistry()

    lateinit var config: Config

    fun init() {
        LOGGER.info("Registering items")
        ITEMS.register()

        LOGGER.info("Registering data components")
        CobbreedingComponents.COMPONENT_TYPES.register()

//        CommandRegistrationEvent.EVENT.register { dispatcher, _, _ ->
//            dispatcher.register(LiteralArgumentBuilder.literal(""))
//        }

        ReloadListenerRegistry.register(PackType.SERVER_DATA, ResourceManagerReloadListener {
            LOGGER.info("Loading configuration")
            loadConfig()
        }, ResourceLocation.fromNamespaceAndPath(MOD_ID, "cobbreeding-data"))

        // Adding and removing players from the incubators cache
        PlatformEvents.SERVER_PLAYER_LOGIN.subscribe {
            INCUBATOR_ABILITIES_REGISTRY.add(it.player)
        }
        PlatformEvents.SERVER_PLAYER_LOGOUT.subscribe {
            INCUBATOR_ABILITIES_REGISTRY.remove(it.player)
        }


    }

    @OptIn(ExperimentalSerializationApi::class)
    fun loadConfig() {
        val configFile = File(CONFIG_PATH)
        configFile.parentFile.mkdirs()

        config = if (configFile.exists()) {
            try {
                Json.decodeFromStream<Config>(configFile.inputStream())
            } catch (e: Exception) {
                LOGGER.error("Config error: ${e.message}")
                LOGGER.warn("Config has been reset.")
                Config()
            }
        } else {
            Config()
        }

        saveConfig()
    }

    @OptIn(ExperimentalSerializationApi::class)
    fun saveConfig() {
        val configFile = File(CONFIG_PATH)
        configFile.parentFile.mkdirs()

        Json.encodeToStream(config, configFile.outputStream())
    }

    fun isEgg(itemStack: ItemStack): Boolean
    {
        return itemStack.item is PokemonEgg
    }

    fun areSameEgg(a: ItemStack, b: ItemStack): Boolean
    {
        if (isEgg(a) && isEgg(b))
        {
            val comp_a = a.getOrDefault(CobbreedingComponents.POKEMON_PROPERTIES.get(), 0)
            val comp_b = b.getOrDefault(CobbreedingComponents.POKEMON_PROPERTIES.get(), 0)
            if (comp_a.equals(comp_b))
                return true
        }
        return false
    }

}
