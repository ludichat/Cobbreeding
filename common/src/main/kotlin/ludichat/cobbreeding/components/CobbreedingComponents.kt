package ludichat.cobbreeding.components

import com.cobblemon.mod.common.api.pokemon.PokemonProperties
import com.mojang.serialization.Codec
import dev.architectury.registry.registries.DeferredRegister
import dev.architectury.registry.registries.RegistrySupplier
import ludichat.cobbreeding.Cobbreeding.MOD_ID
import net.minecraft.core.component.DataComponentType
import net.minecraft.core.registries.Registries

object CobbreedingComponents {
    internal val COMPONENT_TYPES: DeferredRegister<DataComponentType<*>> = DeferredRegister.create(MOD_ID, Registries.DATA_COMPONENT_TYPE)

    @JvmField
    val TIMER: RegistrySupplier<DataComponentType<Int>> = COMPONENT_TYPES.register("timer") {
        DataComponentType.builder<Int>().persistent(Codec.INT).build()
    }

    @JvmField
    val SECOND: RegistrySupplier<DataComponentType<Int>> = COMPONENT_TYPES.register("second") {
        DataComponentType.builder<Int>().persistent(Codec.INT).build()
    }

    @JvmField
    val EGG_INFO: RegistrySupplier<DataComponentType<PokemonEggInfo>> = COMPONENT_TYPES.register("egg_info") {
        DataComponentType.builder<PokemonEggInfo>().persistent(PokemonEggInfo.CODEC).build()
    }

    @JvmField
    val POKEMON_PROPERTIES: RegistrySupplier<DataComponentType<PokemonProperties>> = COMPONENT_TYPES.register("pokemon_properties") {
        DataComponentType.builder<PokemonProperties>().persistent(PokemonProperties.CODEC).build()
    }
}