package ludichat.cobbreeding.components

import com.cobblemon.mod.common.api.moves.MoveTemplate
import com.cobblemon.mod.common.api.pokemon.Natures
import com.cobblemon.mod.common.api.pokemon.PokemonSpecies
import com.cobblemon.mod.common.pokemon.IVs
import com.cobblemon.mod.common.pokemon.Nature
import com.cobblemon.mod.common.pokemon.Species
import com.mojang.serialization.Codec
import com.mojang.serialization.codecs.RecordCodecBuilder
import ludichat.cobbreeding.PastureUtilities.toIVs
import ludichat.cobbreeding.PastureUtilities.toIntArray
import ludichat.cobbreeding.PastureUtilities.toMoves
import net.minecraft.nbt.CompoundTag
import java.util.*

data class PokemonEggInfo(
    val species: Optional<String>,
    val ivs: Optional<IVs>,
    val nature: Optional<Nature>,
    val form: Optional<String>,
    val eggMoves: Set<MoveTemplate>,
    val ability: Optional<String>,
    val pokeballName: Optional<String>,
    val shiny: Boolean,
    val aspects: List<String>
) {
    enum class Keys(val key: String) {
        Species("species"),
        IVs("ivs"),
        Nature("nature"),
        Form("form"),
        EggMoves("egg_moves"),
        Ability("ability"),
        Pokeball("pokeball"),
        Shiny("shiny"),
        Aspects("aspects")
    }

    companion object {
        val CODEC: Codec<PokemonEggInfo> = RecordCodecBuilder.create {
            return@create it.group(
                Codec.STRING.optionalFieldOf(Keys.Species.key).forGetter(PokemonEggInfo::species),
                Codec.INT.listOf().xmap({ list -> list.toIntArray().toIVs() }, { ivs -> ivs.toIntArray().toList() })
                    .optionalFieldOf(Keys.IVs.key).forGetter(PokemonEggInfo::ivs),
                CobblemonCodecs.NATURE.optionalFieldOf(Keys.Nature.key).forGetter(PokemonEggInfo::nature),
                Codec.STRING.optionalFieldOf(Keys.Form.key).forGetter(PokemonEggInfo::form),
                CobblemonCodecs.MOVE_TEMPLATE.listOf().xmap({ list -> list.toSet() }, { set -> set.toList() })
                    .optionalFieldOf(Keys.EggMoves.key, setOf()).forGetter(PokemonEggInfo::eggMoves),
                Codec.STRING.optionalFieldOf(Keys.Ability.key).forGetter(PokemonEggInfo::ability),
                Codec.STRING.optionalFieldOf(Keys.Pokeball.key).forGetter(PokemonEggInfo::pokeballName),
                Codec.BOOL.optionalFieldOf(Keys.Shiny.key, false).forGetter(PokemonEggInfo::shiny),
                Codec.STRING.listOf().optionalFieldOf(Keys.Aspects.key, null).forGetter(PokemonEggInfo::aspects),
            ).apply(it, ::PokemonEggInfo)
        }

        /**
         * Creates a [PokemonEggInfo] object from legacy NBT data so that [ludichat.cobbreeding.PokemonEgg] items
         * created in 1.20.1 don't break.
         */
        @JvmStatic
        fun fromNbt(nbt: CompoundTag): PokemonEggInfo {
            val speciesNbt = nbt.getString(Keys.Species.key)
            val ivsNbt = nbt.getIntArray(Keys.IVs.key)
            val natureNbt = nbt.getString(Keys.Nature.key)
            val formNbt = nbt.getString(Keys.Form.key)
            val eggMovesNbt = nbt.getIntArray(Keys.EggMoves.key)
            val abilityNbt = nbt.getString(Keys.Ability.key)
            val pokeballNbt = nbt.getString(Keys.Pokeball.key)
            val shinyNbt = nbt.getBoolean(Keys.Shiny.key)

            return PokemonEggInfo(
                Optional.ofNullable(speciesNbt),
                Optional.ofNullable(ivsNbt.toIVs()),
                Optional.ofNullable(natureNbt.let { Natures.getNature(it.split(":").last()) }),
                Optional.ofNullable(formNbt),
                eggMovesNbt.toMoves(),
                Optional.ofNullable(abilityNbt),
                Optional.ofNullable(pokeballNbt),
                shinyNbt,
                listOf()
            )
        }
    }
}