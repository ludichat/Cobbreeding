package ludichat.cobbreeding

import com.cobblemon.mod.common.Cobblemon
import com.cobblemon.mod.common.CobblemonItemComponents
import com.cobblemon.mod.common.api.events.CobblemonEvents
import com.cobblemon.mod.common.api.events.pokemon.HatchEggEvent
import com.cobblemon.mod.common.api.pokemon.PokemonProperties
import com.cobblemon.mod.common.api.pokemon.PokemonSpecies
import com.cobblemon.mod.common.api.pokemon.feature.FlagSpeciesFeature
import com.cobblemon.mod.common.api.pokemon.feature.StringSpeciesFeature
import com.cobblemon.mod.common.pokemon.FormData
import ludichat.cobbreeding.components.CobbreedingComponents
import ludichat.cobbreeding.components.PokemonEggInfo
import net.minecraft.core.component.DataComponentType
import net.minecraft.core.component.DataComponents
import net.minecraft.network.chat.Component
import net.minecraft.server.level.ServerPlayer
import net.minecraft.world.entity.Entity
import net.minecraft.world.entity.player.Player
import net.minecraft.world.item.Item
import net.minecraft.world.item.ItemStack
import net.minecraft.world.item.TooltipFlag
import net.minecraft.world.level.Level
import java.util.LinkedList
import kotlin.math.floor

/**
 * Pokémon egg item.
 */
class PokemonEgg(settings: Properties) : Item(settings) {
    companion object {
        const val DEFAULT_TIMER = 600
        const val DEFAULT_SECOND = 0

        val TIMER: DataComponentType<Int> by lazy { CobbreedingComponents.TIMER.get() }
        val SECOND: DataComponentType<Int> by lazy { CobbreedingComponents.SECOND.get() }
        val EGG_INFO: DataComponentType<PokemonEggInfo> by lazy { CobbreedingComponents.EGG_INFO.get() }
        val POKEMON_PROPERTIES: DataComponentType<PokemonProperties> by lazy { CobbreedingComponents.POKEMON_PROPERTIES.get() }
    }

    override fun verifyComponentsAfterLoad(itemStack: ItemStack) {
        super.verifyComponentsAfterLoad(itemStack)

        // Filter out items that already use components
        if (!itemStack.has(TIMER) || !itemStack.has(EGG_INFO)) {
            // Look up and edit data in minecraft:custom_data
            val customData = itemStack.get(DataComponents.CUSTOM_DATA)
            val pokemonItem = itemStack.get(CobblemonItemComponents.POKEMON_ITEM)

            val newData = customData?.update { nbt ->
                // Fix data stolen by Cobblemon, blame Polymeta
                if (pokemonItem != null) {
                    nbt.putString(PokemonEggInfo.Keys.Species.key, pokemonItem.species.path)
                    itemStack.remove(CobblemonItemComponents.POKEMON_ITEM)
                }

                // Read Cobbreeding data and convert to components
                if (nbt.contains("timer")) {
                    val timer = nbt.getInt("timer")
                    itemStack.set(TIMER, timer)
                }
                if (nbt.contains(PokemonEggInfo.Keys.Species.key)) {
                    val info = PokemonEggInfo.fromNbt(nbt)
                    itemStack.set(POKEMON_PROPERTIES, EggInfoToProperties(info))
                }

                // Remove Cobbreeding data
                nbt.remove("timer")
                PokemonEggInfo.Keys.entries.forEach { nbt.remove(it.key) }
            }
            itemStack.set(DataComponents.CUSTOM_DATA, newData)
        }
        if (itemStack.has(EGG_INFO))
        {
            itemStack.set(POKEMON_PROPERTIES, EggInfoToProperties(itemStack.get(EGG_INFO)!!))
        }
    }

    private fun EggInfoToProperties(eggInfo: PokemonEggInfo): PokemonProperties
    {
        val pokemonProperties = PokemonProperties()

        eggInfo.species.ifPresent { species ->
            val pokemon = PokemonSpecies.getByName(species)
            if (pokemon != null) {
                pokemonProperties.species = pokemon.showdownId()
                pokemonProperties.form = eggInfo.form.toString()
                eggInfo.ivs.ifPresent { pokemonProperties.ivs = it }
                eggInfo.nature.ifPresent { pokemonProperties.nature = it.name.toString() }
                eggInfo.ability.ifPresent { pokemonProperties.ability = it }
                eggInfo.pokeballName.ifPresent { pokemonProperties.pokeball = it }
                pokemonProperties.shiny = eggInfo.shiny
                pokemonProperties.moves = LinkedList()
                for(move in eggInfo.eggMoves)
                {
                    (pokemonProperties.moves as LinkedList<String>).addLast(move.name)
                }
                pokemonProperties.aspects = eggInfo.aspects.toSet()
            }
        }

        return pokemonProperties
    }

    override fun appendHoverText(
        stack: ItemStack,
        context: TooltipContext,
        lines: MutableList<Component>,
        type: TooltipFlag
    ) {
        super.appendHoverText(stack, context, lines, type)

        val properties = stack.get(POKEMON_PROPERTIES)
        if (properties!!.species != null)
        {
            val pokemon = PokemonSpecies.getByName(properties.species!!)
            if (pokemon != null) {
                lines.add(pokemon.translatedName)

                if(properties.form != null)
                {
                    val data = pokemon.forms.find { it.formOnlyShowdownId() == properties.form }
                    if (data is FormData && data.name != "Normal") lines.add(Component.literal(data.name))
                }
            }
        }
        else
        {
            lines.add(Component.literal("Bad egg"))
        }

        val timer = stack.getOrDefault(TIMER, DEFAULT_TIMER)
        lines.add(Component.literal(ticksToTime(timer)))
    }

    override fun inventoryTick(stack: ItemStack, world: Level, entity: Entity, slot: Int, selected: Boolean) {
        super.inventoryTick(stack, world, entity, slot, selected)

        if (entity is Player) {
            // Handling a timer to play this action every second instead of every tick
            val second = stack.getOrDefault(SECOND, DEFAULT_SECOND)
            stack.set(SECOND, second + 1)
            if (second < 20) return
            stack.set(SECOND, 0)

            val timer = stack.getOrDefault(TIMER, DEFAULT_TIMER)
            stack.set(TIMER, timer - 20)

            // Egg hatches faster is the player has a pokemon with an ability increasing hatching speed
            if(Cobbreeding.INCUBATOR_ABILITIES_REGISTRY.shouldHatchFaster(entity.stringUUID))
                stack.set(TIMER, timer - 40)

            if (timer <= 0 && !world.isClientSide) {
                val pokemonProperties = stack.get(POKEMON_PROPERTIES)
                val species = pokemonProperties!!.species

                if(species != null && PokemonSpecies.getByName(species) != null)
                {
                    // Resolving the form
                    if (pokemonProperties.form != null)
                    {
                        PokemonSpecies.getByName(species)!!.forms.find { it.formOnlyShowdownId() == pokemonProperties.form }?.run {
                            aspects.forEach {
                                // alternative form
                                pokemonProperties.customProperties.add(FlagSpeciesFeature(it, true))
                                // regional bias
                                pokemonProperties.customProperties.add(
                                        StringSpeciesFeature(
                                                "region_bias",
                                                it.split("-").last()
                                        )
                                )
                                // Basculin wants to be special
                                // We're handling aspects now but some form handling should be kept to prevent
                                // legitimate abilities to be flagged as forced
                                pokemonProperties.customProperties.add(
                                        StringSpeciesFeature(
                                                "fish_stripes",
                                                it.removeSuffix("striped")
                                        )
                                )
                            }
                        }
                    }

                    // Creating the pokemon and hatching the egg
                    try {
                        CobblemonEvents.HATCH_EGG_PRE.post(HatchEggEvent.Pre(pokemonProperties, entity as ServerPlayer))
                        val pokemon = pokemonProperties.create()
                        pokemon.setFriendship(120)

                        // Adds the hatched pokémon to the party/pc and registers it in the pokedex
                        Cobblemon.storage.getParty(entity.uuid, entity.registryAccess()).add(pokemon)
                        Cobblemon.playerDataManager.getPokedexData(entity.uuid).catch(pokemon)

                        CobblemonEvents.HATCH_EGG_POST.post(HatchEggEvent.Post(pokemonProperties, entity))
                    } catch (e: Exception) {
                        Cobbreeding.LOGGER.error("Egg hatching failed: ${e.message}\n${e.stackTrace}")
                        entity.sendSystemMessage(Component.translatable("cobbreeding.msg.egg_hatch.fail"))
                    } finally {
                        stack.shrink(1)
                    }
                }
                else
                {
                    entity.sendSystemMessage(Component.literal("Couldn't resolve pokemon species to hatch, deleting egg."))
                    stack.shrink(1)
                }
            }
        }
    }

    /**
     * Convert ticks to minutes and seconds
     * @return string representing time.
     */
    private fun ticksToTime(ticks: Int): String {
        val minutes = floor((ticks / 1200).toFloat()).toInt()
        val seconds = "%02d".format(floor((ticks % 1200 / 20).toFloat()).toInt())


        return if (minutes <= 0)
            seconds
        else
            "$minutes:$seconds"
    }
}
