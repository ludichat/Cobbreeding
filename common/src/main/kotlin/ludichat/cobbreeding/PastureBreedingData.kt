package ludichat.cobbreeding

import net.minecraft.world.item.ItemStack
import net.minecraft.core.NonNullList

class PastureBreedingData(var time: Int, var egg: NonNullList<ItemStack>) {
    companion object {
        @JvmField
        val registry: Map<Int, PastureBreedingData> = hashMapOf()
    }
}
