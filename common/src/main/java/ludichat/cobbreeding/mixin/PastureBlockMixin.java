package ludichat.cobbreeding.mixin;

import com.cobblemon.mod.common.block.PastureBlock;
import ludichat.cobbreeding.*;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.Container;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import ludichat.cobbreeding.CustomProperties;
import ludichat.cobbreeding.PastureBreedingData;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;


@Mixin(PastureBlock.class)
public abstract class PastureBlockMixin extends Block {
    @Unique
    private static final BooleanProperty HAS_EGG = CustomProperties.HAS_EGG;

    public PastureBlockMixin(Properties settings) {
        super(settings);
    }

    @Inject(at = @At("TAIL"), method = "<init>")
    private void init(BlockBehaviour.Properties properties, CallbackInfo ci) {
        registerDefaultState(defaultBlockState().setValue(HAS_EGG, false));
    }

    @Inject(at = @At("TAIL"), method = "createBlockStateDefinition")
    private void appendProperties(StateDefinition.Builder<Block, BlockState> builder, CallbackInfo ci) {
        builder.add(HAS_EGG);
    }

    @Inject(at = @At("HEAD"), method = "useWithoutItem", cancellable = true)
    private void onUse(BlockState state, Level world, BlockPos pos, Player player, BlockHitResult hit, CallbackInfoReturnable<InteractionResult> cir) {
        if (!world.isClientSide) {
            boolean is_bottom_part = state.getValue(PastureBlock.Companion.getPART()) == PastureBlock.PasturePart.BOTTOM;
            Container pastureInventory = (Container) world.getBlockEntity(pos);

            if (pastureInventory != null && !pastureInventory.getItem(0).isEmpty() && is_bottom_part) {
                // Get the egg
                player.getInventory().placeItemBackInInventory(pastureInventory.getItem(0));
                pastureInventory.removeItemNoUpdate(0);
                player.getInventory().tick();

                cir.setReturnValue(InteractionResult.SUCCESS);
            }
        }
    }

    @Inject(at = @At("HEAD"), method = "playerWillDestroy")
    private void onBroken(Level world, BlockPos pos, BlockState state, Player player, CallbackInfoReturnable<BlockState> cir) {
        if (!world.isClientSide) {
            int hash = state.hashCode();

            PastureBreedingData.registry.remove(hash);
        }
    }
}
