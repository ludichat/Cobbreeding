package ludichat.cobbreeding.fabric

import ludichat.cobbreeding.Cobbreeding
import ludichat.cobbreeding.CobbreedingClient.init
import net.fabricmc.api.ClientModInitializer
import net.fabricmc.fabric.api.resource.ResourceManagerHelper
import net.fabricmc.fabric.api.resource.ResourcePackActivationType
import net.fabricmc.loader.api.FabricLoader
import net.minecraft.network.chat.Component
import net.minecraft.resources.ResourceLocation

@Suppress("Unused")
class CobbreedingClient : ClientModInitializer {
    override fun onInitializeClient() {
        init()
        ResourceManagerHelper.registerBuiltinResourcePack(
            ResourceLocation.fromNamespaceAndPath("cobbreeding", "pasturefix"),
            FabricLoader.getInstance().getModContainer(Cobbreeding.MOD_ID).get(),
            Component.literal("Pasture Block Fix"),
            ResourcePackActivationType.ALWAYS_ENABLED
        )
    }
}