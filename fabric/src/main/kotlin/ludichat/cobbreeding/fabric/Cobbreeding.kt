package ludichat.cobbreeding.fabric

import ludichat.cobbreeding.Cobbreeding.init
import net.fabricmc.api.ModInitializer

@Suppress("unused")
class Cobbreeding : ModInitializer {
    override fun onInitialize() {
        init()
    }
}
